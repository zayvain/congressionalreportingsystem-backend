package com.crsystem.springjwt.controllers;

import com.crsystem.springjwt.exception.ResourceNotFoundException;
import com.crsystem.springjwt.models.District;
import com.crsystem.springjwt.models.Municipality;
import com.crsystem.springjwt.repository.DistrictRepository;
import com.crsystem.springjwt.repository.MunicipalityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/test/municipalities")
@CrossOrigin(origins = {"http://localhost:4200"})
public class MunicipalityController {

    @Autowired
    private MunicipalityRepository municipalityRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @GetMapping("districts/all")
    public List<Municipality> getAll(){
        return municipalityRepository.findAll();
    }

    @GetMapping("/districts/{districtId}")
    public List<Municipality> getByDistrictId(@PathVariable(value = "districtId") Long districtId){
        return municipalityRepository.findByDistrictId(districtId);
    }

    @GetMapping("/districts/{districtId}/page")
    public Page<Municipality> getAllMunicipalitiesByDistrictId(@PathVariable(value = "districtId") Long districtId,
                                                               Pageable pageable) {
        return municipalityRepository.findByDistrictId(districtId, pageable);
    }

    @PostMapping("/districts/{districtId}")
    public Municipality createMunicipality(@PathVariable (value = "districtId") Long districtId,
                                           @Valid @RequestBody Municipality municipality) {
        return districtRepository.findById(districtId).map(district -> {
            municipality.setDistrict(district);
            return municipalityRepository.save(municipality);
        }).orElseThrow(() -> new ResourceNotFoundException("DistrictId " + districtId + " not found"));
    }

    @PutMapping("/districts/{districtId}/municipalities/{municipalityId}")
    public Municipality updateMunicipality(@PathVariable (value = "districtId") Long districtId,
                                           @PathVariable (value = "municipalityId") Long municipalityId,
                                           @Valid @RequestBody Municipality municipalityRequest) {
        if(!districtRepository.existsById(districtId)) {
            throw new ResourceNotFoundException("DistrictId " + districtId + " not found");
        }

        return municipalityRepository.findById(municipalityId).map(municipality -> {
            municipality.setName(municipalityRequest.getName());
            return municipalityRepository.save(municipality);
        }).orElseThrow(() -> new ResourceNotFoundException("municipalityId " + municipalityId + "not found"));
    }

    @DeleteMapping("/districts/{districtId}/municipalities/{municipalityId}")
    public ResponseEntity<?> deleteMunicipality(@PathVariable (value = "districtId") Long districtId,
                                                @PathVariable (value = "municipalityId") Long municipalityId) {
        return municipalityRepository.findByIdAndDistrictId(municipalityId, districtId).map(municipality -> {
            municipalityRepository.delete(municipality);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("municipality not found with id " + municipalityId + " and districtId " + districtId));
    }
}
