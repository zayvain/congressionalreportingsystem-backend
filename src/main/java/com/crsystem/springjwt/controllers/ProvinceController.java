package com.crsystem.springjwt.controllers;

import com.crsystem.springjwt.exception.ResourceNotFoundException;
import com.crsystem.springjwt.models.Province;
import com.crsystem.springjwt.repository.ProvinceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/test/provinces")
@CrossOrigin(origins = {"http://localhost:4200"})
public class ProvinceController {

    @Autowired
    private ProvinceRepository provinceRepository;

    @GetMapping("/all")
    public List<Province> getAll(){
        return provinceRepository.findAll();
    }

    @GetMapping("/page")
    public Page<Province> getAllProvinces(Pageable pageable) {
        return provinceRepository.findAll(pageable);
    }

    @PostMapping("")
    public Province createProvince(@Valid @RequestBody Province province) {
        return provinceRepository.save(province);
    }

    @PutMapping("/{provinceId}")
    public Province updateProvince(@PathVariable Long provinceId, @Valid @RequestBody Province provinceRequest) {
        return provinceRepository.findById(provinceId).map(province -> {
            province.setName(provinceRequest.getName());
            province.setServedbenes(provinceRequest.getServedbenes());
            province.setTargetbenes(provinceRequest.getTargetbenes());
            province.setAmountallocated(provinceRequest.getAmountallocated());
            province.setAmountdisbursed(provinceRequest.getAmountdisbursed());
            return provinceRepository.save(province);
        }).orElseThrow(() -> new ResourceNotFoundException("Province Id " + provinceId + " not found"));
    }

    @DeleteMapping("/{provinceId}")
    public ResponseEntity<?> deleteProvince(@PathVariable Long provinceId) {
        return provinceRepository.findById(provinceId).map(province -> {
            provinceRepository.delete(province);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Province Id " + provinceId + " not found"));
    }

}
