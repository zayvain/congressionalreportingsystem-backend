package com.crsystem.springjwt.repository;

import com.crsystem.springjwt.models.District;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DistrictRepository  extends JpaRepository<District, Long> {
    Page<District> findByProvinceId(Long provinceId, Pageable pageable);
    List<District> findByProvinceId(Long provinceId);
    Optional<District> findByIdAndProvinceId(Long id, Long provinceId);
}

