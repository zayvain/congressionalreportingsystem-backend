package com.crsystem.springjwt.repository;

import com.crsystem.springjwt.models.Program;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface ProgramRepository extends JpaRepository<Program, Long>,
        QuerydslPredicateExecutor<Program> {

    Program findById(long id);
}
