package com.crsystem.springjwt.repository;


import com.crsystem.springjwt.models.Province;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProvinceRepository  extends JpaRepository<Province, Long> {
}

