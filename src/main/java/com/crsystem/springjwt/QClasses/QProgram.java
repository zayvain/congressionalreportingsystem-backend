package com.crsystem.springjwt.QClasses;

import com.crsystem.springjwt.models.Program;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

public class QProgram extends EntityPathBase<Program> {

    private static final long serialVersionUID = -1682007758L;
    public static final QProgram program = new QProgram("program");
    public final StringPath name = this.createString("name");
    public final NumberPath<Long> id = this.createNumber("id", Long.class);
    public final StringPath department = this.createString("department");
    public final StringPath target = this.createString("target");
    public final StringPath funds = this.createString("funds");


    public QProgram(String variable) {
        super(Program.class, PathMetadataFactory.forVariable(variable));
    }

    public QProgram(Path<? extends Program> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProgram(PathMetadata metadata) {
        super(Program.class, metadata);
    }
}
