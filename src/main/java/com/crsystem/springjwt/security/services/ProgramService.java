package com.crsystem.springjwt.security.services;

import com.crsystem.springjwt.QClasses.QProgram;
import com.crsystem.springjwt.models.Program;
import com.crsystem.springjwt.repository.ProgramRepository;
import com.querydsl.core.BooleanBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProgramService {

    @Autowired
    private ProgramRepository programRepository;

    // GET ALL - /representatives?page=(default = 0) -&firstName -&lastName -&occupation
    public Page<Program> getAll(Pageable page, String name, String department,String target,String funds){
        BooleanBuilder b = new BooleanBuilder();
        QProgram qProgram =   QProgram.program;
        if(name != null) {
            b.or(qProgram.name.toLowerCase().contains(name.toLowerCase()));
        }
        if(department != null) {
            b.or(qProgram.department.toLowerCase().contains(department.toLowerCase()));
        }
        if(target != null) {
            b.or(qProgram.target.toLowerCase().contains(target.toLowerCase()));
        }
        if(funds != null) {
            b.or(qProgram.funds.toLowerCase().contains(funds.toLowerCase()));
        }
        return programRepository.findAll(b, page);
    }


    // GET - /representatives/all
    public List<Program> getAll() {
        return programRepository.findAll();
    }

    // GET - /representatives/{id}
    public Program findById(long id) {
        Program program = programRepository.findById(id);
        if(program == null) {
            throw new RuntimeException("Program not found");
        }
        return program;
    }

    // POST - /representatives/
    public Program create(Program program) {
        return programRepository.save(program);
    }

    // PUT - /representatives/{id}
    public Program update(Program program, long id) {
        program.setId(id);
        return programRepository.save(program);
    }

    // DELETE - /representatives/{id}
    public Program deleteById(long id) {
        Program program = findById(id);
        if(program == null) {
            throw new RuntimeException("program does not exist");
        }
        programRepository.deleteById(id);
        return program;
    }
}
