package com.crsystem.springjwt.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="municipality")
public class Municipality {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    private double targetbenes;
    private double servedbenes;
    private double amountallocated;
    private double amountdisbursed;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "district_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    @JsonProperty("district_id")
    private District district;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public double getTargetbenes() {
        return targetbenes;
    }

    public void setTargetbenes(double targetbenes) {
        this.targetbenes = targetbenes;
    }

    public double getServedbenes() {
        return servedbenes;
    }

    public void setServedbenes(double servedbenes) {
        this.servedbenes = servedbenes;
    }

    public double getAmountallocated() {
        return amountallocated;
    }

    public void setAmountallocated(double amountallocated) {
        this.amountallocated = amountallocated;
    }

    public double getAmountdisbursed() {
        return amountdisbursed;
    }

    public void setAmountdisbursed(double amountdisbursed) {
        this.amountdisbursed = amountdisbursed;
    }
}
