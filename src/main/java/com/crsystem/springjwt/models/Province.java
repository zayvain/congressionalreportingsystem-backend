package com.crsystem.springjwt.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="province")
public class Province  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 100)
    private String name;

    private String governor;
    private double targetbenes;
    private double servedbenes;
    private double amountallocated;
    private double amountdisbursed;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTargetbenes() {
        return targetbenes;
    }

    public void setTargetbenes(double targetbenes) {
        this.targetbenes = targetbenes;
    }

    public double getServedbenes() {
        return servedbenes;
    }

    public void setServedbenes(double servedbenes) {
        this.servedbenes = servedbenes;
    }

    public double getAmountallocated() {
        return amountallocated;
    }

    public void setAmountallocated(double amountallocated) {
        this.amountallocated = amountallocated;
    }

    public double getAmountdisbursed() {
        return amountdisbursed;
    }

    public void setAmountdisbursed(double amountdisbursed) {
        this.amountdisbursed = amountdisbursed;
    }

    public String getGovernor() {
        return governor;
    }

    public void setGovernor(String governor) {
        this.governor = governor;
    }
}
